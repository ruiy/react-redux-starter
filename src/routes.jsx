/**
 * Created by ruiy on 10/11/2016.
 */

import App from './views/App.jsx'

const Routes = [{
    path: '/', component: App,
    childRoutes: [{}]
}]

export default Routes