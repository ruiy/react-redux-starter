/**
 * Created by ruiy on 10/11/2016.
 */

'use strict'

import 'normalize.css'
import './assets/style/foundation.css'
import './assets/style/style.less'
import React from 'react'
import ReactDOM from 'react-dom'
import FastClick from 'fastclick'
import {browserHistory} from 'react-router'
import {syncHistoryWithStore} from 'react-router-redux'
import * as promise from 'es6-promise'
import Root from './views/Root.jsx'
import configureStore from './store/configureStore.jsx'

//noinspection JSUnresolvedFunction
promise.polyfill()

const main = function () {
    FastClick.attach(document.body)

    const store = configureStore()

    const history = syncHistoryWithStore(browserHistory, store)

    if (typeof window != 'undefined') {
        ReactDOM.render(
            <Root store={store} history={history}/>, document.getElementById('app')
        )
    }
}

main()