/**
 * Created by ruiy on 10/11/2016.
 */

//noinspection JSUnresolvedVariable
if (process.env.NODE_ENV === 'production') {
    module.exports = require('./configureStore.dist.jsx')
} else {
    module.exports = require('./configureStore.dev.jsx')
}