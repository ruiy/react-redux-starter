/**
 * Created by ruiy on 10/11/2016.
 */

//noinspection JSUnresolvedVariable
import {createStore, applyMiddleware, compose} from 'redux'
//noinspection JSUnresolvedVariable
import {browserHistory} from 'react-router'
import {routerMiddleware} from 'react-router-redux'
import thunkMiddleware from 'redux-thunk'
import rootReducer from '../reducers'

const middleware = routerMiddleware(browserHistory)
//noinspection JSUnresolvedVariable,JSUnresolvedFunction
const finalCreateStore = compose(
    applyMiddleware(thunkMiddleware, middleware),
    window.devToolsExtension ? window.devToolsExtension() : f => f
)(createStore)

export default function configureStore(initialState) {
    const store = finalCreateStore(rootReducer, initialState)

    if (module.hot) {
        module.hot.accept('../reducers', () => {
            const nextRootReducer = require('../reducers')
            store.replaceReducer(nextRootReducer)
        })
    }

    return store
}