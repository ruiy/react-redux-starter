/**
 * Created by ruiy on 10/11/2016.
 */

//noinspection JSUnresolvedVariable
import Immutable from 'immutable'

const AppState = Immutable.fromJS({
    appConfig: {
        locale: 'en_US'
    }
})

export default AppState