/**
 * Created by ruiy on 10/11/2016.
 */

//noinspection JSUnresolvedVariable
import {createStore, applyMiddleware, compose} from 'redux'
import {routerMiddleware} from 'react-router-redux'
//noinspection JSUnresolvedVariable
import {browserHistory} from 'react-router'
import thunkMiddleware from 'redux-thunk'
import rootReducer from '../reducers'

const middleware = routerMiddleware(browserHistory)
const finalCreateStore = compose(
    applyMiddleware(thunkMiddleware, middleware)
)(createStore)

export default function configureStore(initialState) {
    return finalCreateStore(rootReducer, initialState)
}