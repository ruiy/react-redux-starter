/**
 * Created by ruiy on 10/11/2016.
 */

//noinspection JSUnresolvedVariable
import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import {APP_NAME} from '../utils/Constants.jsx'
import {changeLocale} from '../actions/AppAction.jsx'

class App extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        const {dispatch} = this.props
        dispatch(changeLocale('en_US'))
    }

    render() {
        const { children } = this.props
        return (
            <div className="container">
                <h3>{APP_NAME}</h3>
                { children }
            </div>
        )
    }
}
App.propTypes = {
    children: PropTypes.node
}

const mapStateToProps = (state) => ({})

export default connect(mapStateToProps)(App)