/**
 * Created by ruiy on 10/11/2016.
 */

//noinspection JSUnresolvedVariable
import React, {Component, PropTypes} from 'react'
import {Provider} from 'react-redux'
//noinspection JSUnresolvedVariable
import {Router} from 'react-router'
//noinspection JSUnresolvedVariable
import Routes from '../routes.jsx'

export default class Root extends Component {
    render() {
        const {store, history} = this.props
        return (
            <Provider store={store}>
                <Router routes={Routes} history={history}/>
            </Provider>
        )
    }
}