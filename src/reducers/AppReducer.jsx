/**
 * Created by ruiy on 10/11/2016.
 */

import * as AppActions from '../actions/AppAction.jsx'
import AppState from '../store/AppState.jsx'

let appConfigState = AppState.get('appConfig')
export function appConfig(state = appConfigState, action) {
    switch (action.type) {
        case AppActions.CHANGE_LOCALE:
            return state.set('locale', action.locale)
        default:
            return state
    }
}