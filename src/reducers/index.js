/**
 * Created by ruiy on 10/11/2016.
 */

import {routerReducer} from 'react-router-redux'
//noinspection JSUnresolvedVariable
import {combineReducers} from 'redux'

const rootReducer = combineReducers({
    routing: routerReducer
})

export default rootReducer