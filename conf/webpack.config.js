/**
 * Created by ruiy on 03/11/2016.
 * Configuration for development environment
 */

var webpack = require('webpack')
var path = require('path')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var HtmlWebpackPlugin = require('html-webpack-plugin')

var config = {
    addVendor: function (type, name, path) {
        this.resolve.alias[name] = path
        this.module.noParse.push(new RegExp('^' + name + '$'))
        if (type == 'js') {
            this.entry.vendors.push(name)
        }
    },
    devServer: {
        historyApiFallback: true,
        hot: true,
        inline: true,
        progress: true,
        contentBase: './build/Development',
        host: '0.0.0.0',
        port: 8080
    },
    entry: {
        app: [
            'webpack/hot/dev-server',
            'webpack-dev-server/client?http://localhost:8080/',
            './src/main.jsx'
        ],
        vendors: []
    },
    resolve: {
        alias: {}
    },
    output: {
        path: './build/Development',
        filename: '[name].js',
        publicPath: 'http://localhost:8080/build/Development/'
    },
    module: {
        noParse: [],
        loaders: [
            {test: /\.(js|jsx)$/, loader: 'babel', include: /src/},
            {test: /\.(png|jpg)$/, loader: 'url-loader'},
            {test: /\.gif$/, loader: 'url-loader?mimetype=image/gif'},
            {test: /\.less$/, loader: 'style-loader!css-loader!less-loader'},
            {test: /\.css$/, loader: ExtractTextPlugin.extract('style-loader', 'css-loader')},
            {
                test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: "url-loader?limit=100000&mimetype=application/font-woff"
            },
            {test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'file-loader'}
        ]
    },
    plugins: [
        new webpack.BannerPlugin('Created by ruiy'),
        new webpack.NoErrorsPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js'),
        new webpack.optimize.DedupePlugin(),
        new ExtractTextPlugin('app.css'),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': '"development"'
        }),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: './src/assets/page/index.dev.tmpl.html'
        })
    ]
}

module.exports = config